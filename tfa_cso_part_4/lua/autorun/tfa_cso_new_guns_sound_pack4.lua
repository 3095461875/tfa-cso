--TFA.AddFireSound( "Gun.Fire", "weapons/tfa_cso/gun/fire.wav", false, "^" )
--TFA.AddWeaponSound( "Gun.Reload", "weapons/tfa_cso/gun/reload.wav" )

--Gunkata
TFA.AddFireSound( "Gunkata.Fire", "weapons/tfa_cso/gunkata/fire.wav", false, "^" )
TFA.AddFireSound( "Gunkata.Skill_Explode", "weapons/tfa_cso/gunkata/skill_last_exp.wav", false, "^" )
TFA.AddWeaponSound( "Gunkata.Reload", "weapons/tfa_cso/gunkata/reload.wav" )
TFA.AddWeaponSound( "Gunkata.Reload2", "weapons/tfa_cso/gunkata/reload2.wav" )
TFA.AddWeaponSound( "Gunkata.Draw2", "weapons/tfa_cso/gunkata/draw2.wav" )
TFA.AddWeaponSound( "Gunkata.Draw", "weapons/tfa_cso/gunkata/draw.wav" )
TFA.AddWeaponSound( "Gunkata.Idle", "weapons/tfa_cso/gunkata/idle.wav" )
TFA.AddWeaponSound( "Gunkata.Skill1", "weapons/tfa_cso/gunkata/skill_01.wav" )
TFA.AddWeaponSound( "Gunkata.Skill2", "weapons/tfa_cso/gunkata/skill_02.wav" )
TFA.AddWeaponSound( "Gunkata.Skill3", "weapons/tfa_cso/gunkata/skill_03.wav" )
TFA.AddWeaponSound( "Gunkata.Skill4", "weapons/tfa_cso/gunkata/skill_04.wav" )
TFA.AddWeaponSound( "Gunkata.Skill5", "weapons/tfa_cso/gunkata/skill_05.wav" )
TFA.AddWeaponSound( "Gunkata.Skilllast", "weapons/tfa_cso/gunkata/skill_last.wav" )
TFA.AddWeaponSound( "Gunkata.SkilllastExp", "weapons/tfa_cso/gunkata/skill_last_exp.wav" )
TFA.AddWeaponSound( "Gunkata.Hit1", "weapons/tfa_cso/gunkata/hit1.wav" )
TFA.AddWeaponSound( "Gunkata.Hit2", "weapons/tfa_cso/gunkata/hit2.wav" )

--Laserfist
TFA.AddFireSound( "Laserfist.FireA", "weapons/tfa_cso/laserfist/fire_a.wav", false, "^" )
TFA.AddFireSound( "Laserfist.FireB", "weapons/tfa_cso/laserfist/fire_b.wav", false, "^" )
TFA.AddWeaponSound( "Laserfist.ClipIn1", "weapons/tfa_cso/laserfist/clipin1.wav" )
TFA.AddWeaponSound( "Laserfist.ClipIn2", "weapons/tfa_cso/laserfist/clipin2.wav" )
TFA.AddWeaponSound( "Laserfist.ClipOut", "weapons/tfa_cso/laserfist/clipout.wav" )
TFA.AddWeaponSound( "Laserfist.Draw", "weapons/tfa_cso/laserfist/draw.wav" )
TFA.AddWeaponSound( "Laserfist.Idle", "weapons/tfa_cso/laserfist/idle.wav" )
TFA.AddWeaponSound( "Laserfist.Charge", "weapons/tfa_cso/laserfist/charge.wav" )
TFA.AddWeaponSound( "Laserfist.Empty_End", "weapons/tfa_cso/laserfist/shoot_empty_end" )
TFA.AddWeaponSound( "Laserfist.Empty_Loop", "weapons/tfa_cso/laserfist/shoot_empty_loop.wav" )
TFA.AddWeaponSound( "Laserfist.Boom", "weapons/tfa_cso/laserfist/shootb_exp.wav" )
TFA.AddWeaponSound( "Laserfist.Ready", "weapons/tfa_cso/laserfist/shootb_ready.wav" )
TFA.AddWeaponSound( "Laserfist.ShootB_Shoot", "weapons/tfa_cso/laserfist/shootb_shoot.wav" )
TFA.AddWeaponSound( "Laserfist.ShootB_Loop", "weapons/tfa_cso/laserfist/shootb_loop.wav" )

--Holy Bomb
TFA.AddFireSound ( "Holybomb.Explode", "weapons/tfa_cso/holybomb/explode.wav", false, "^" )
TFA.AddWeaponSound ( "HolyBomb.PullPin", "weapons/tfa_cso/holybomb/pullpin.wav" )
TFA.AddWeaponSound ( "HolyBomb.Draw", "weapons/tfa_cso/holybomb/draw.wav" )

--Dark Legacy Luger
TFA.AddFireSound ( "Luger_Legacy.Fire", "weapons/tfa_cso/luger_legacy/fire.wav", false, "^" )

--Trinity Grenade
TFA.AddFireSound ( "Trinity.ExplodeRed", "weapons/tfa_cso/trinity/red_explode.wav", false, "^" )
TFA.AddFireSound ( "Trinity.ExplodeGreen", "weapons/tfa_cso/trinity/green_explode.wav", false, "^" )
TFA.AddFireSound ( "Trinity.ExplodeWhite", "weapons/tfa_cso/trinity/white_explode.wav", false, "^" )
--TFA.AddWeaponSound ( "Trinity.IdleRed", "weapons/tfa_cso/trinity/red_idle.wav" )
--TFA.AddWeaponSound ( "Trinity.IdleGreen", "weapons/tfa_cso/trinity/green_idle.wav" )
--TFA.AddWeaponSound ( "Trinity.IdleWhite", "weapons/tfa_cso/trinity/white_idle.wav" )
--This fixes idle sounds looping over themselves

sound.Add({
	['name'] = "Trinity.IdleRed",
	['channel'] = CHAN_WEAPON,
	['sound'] = { "weapons/tfa_cso/trinity/red_idle.wav"},
	['pitch'] = {100,100}
})
sound.Add({
	['name'] = "Trinity.IdleGreen",
	['channel'] = CHAN_WEAPON,
	['sound'] = { "weapons/tfa_cso/trinity/green_idle.wav"},
	['pitch'] = {100,100}
})
sound.Add({
	['name'] = "Trinity.IdleWhite",
	['channel'] = CHAN_WEAPON,
	['sound'] = { "weapons/tfa_cso/trinity/white_idle.wav"},
	['pitch'] = {100,100}
})

TFA.AddWeaponSound ( "Trinity.TransformRed", "weapons/tfa_cso/trinity/red_transform.wav" )
TFA.AddWeaponSound ( "Trinity.TransformGreen", "weapons/tfa_cso/trinity/green_transform.wav" )
TFA.AddWeaponSound ( "Trinity.TransformWhite", "weapons/tfa_cso/trinity/white_transform.wav" )
TFA.AddWeaponSound ( "Trinity.TransformBase", "weapons/tfa_cso/trinity/transform_base.wav" )
TFA.AddWeaponSound ( "Trinity.PullPin", "weapons/tfa_cso/trinity/pullpin.wav" )
TFA.AddWeaponSound ( "Trinity.Draw", "weapons/tfa_cso/trinity/draw.wav" )

--Zhubajie Minigun
TFA.AddFireSound( "Zhubajie.Fire", "weapons/tfa_cso/monkeywpset1/fire.wav", false, "^" )
TFA.AddWeaponSound( "Zhubajie.Spindown", "weapons/tfa_cso/monkeywpset1/spindown.wav" )
TFA.AddWeaponSound( "Zhubajie.ClipIn", "weapons/tfa_cso/monkeywpset1/clipin.wav" )
TFA.AddWeaponSound( "Zhubajie.ClipOut", "weapons/tfa_cso/monkeywpset1/clipout.wav" )
TFA.AddWeaponSound( "Zhubajie.ClipOut1", "weapons/tfa_cso/monkeywpset1/clipout1.wav" )
TFA.AddWeaponSound( "Zhubajie.ClipOut2", "weapons/tfa_cso/monkeywpset1/clipout2.wav" )

--Sha Wujing Dual Handgun
TFA.AddFireSound( "Wujing.Fire", "weapons/tfa_cso/monkeywpset2/fire.wav", false, "^" )
TFA.AddWeaponSound( "Wujing.Draw", "weapons/tfa_cso/monkeywpset2/draw.wav" )
TFA.AddWeaponSound( "Wujing.ClipIn", "weapons/tfa_cso/monkeywpset2/clipin.wav" )
TFA.AddWeaponSound( "Wujing.ClipOut", "weapons/tfa_cso/monkeywpset2/clipout.wav" )

--X-Tracker
TFA.AddFireSound( "XTracker.Fire", "weapons/tfa_cso/xtracker/fire.wav", false, "^" )
TFA.AddFireSound( "XTracker.ShootB", "weapons/tfa_cso/xtracker/shootb.wav", false, "^" )
TFA.AddFireSound( "XTracker.Exp", "weapons/tfa_cso/xtracker/exp.wav", false, "^" )
TFA.AddWeaponSound( "XTracker.Reload", "weapons/tfa_cso/xtracker/reload.wav" )
TFA.AddWeaponSound( "XTracker.ZoomIn", "weapons/tfa_cso/xtracker/zoom_in.wav" )
TFA.AddWeaponSound( "XTracker.ZoomOut", "weapons/tfa_cso/xtracker/zoom_out.wav" )
TFA.AddWeaponSound( "XTracker.ScopeOn", "weapons/tfa_cso/xtracker/scope_on.wav" )
TFA.AddWeaponSound( "XTracker.Shoot_On1", "weapons/tfa_cso/xtracker/shootb_on1.wav" )
TFA.AddWeaponSound( "XTracker.Shoot_On2", "weapons/tfa_cso/xtracker/shootb_on2.wav" )
TFA.AddWeaponSound( "XTracker.Beep", "weapons/tfa_cso/xtracker/beep.wav" )
TFA.AddWeaponSound( "XTracker.Draw", "weapons/tfa_cso/xtracker/draw.wav" )

--Janus 1
TFA.AddFireSound( "Janus1.Fire", "weapons/tfa_cso/janus1/fire.wav", false, "^" )
TFA.AddFireSound( "Janus1.Fire2", "weapons/tfa_cso/janus1/fire2.wav", false, "^" )
TFA.AddFireSound( "Janus1.Exp", "weapons/tfa_cso/janus1/exp.wav", false, "^" )
TFA.AddWeaponSound( "Janus1.Change1", "weapons/tfa_cso/janus1/change1.wav" )
TFA.AddWeaponSound( "Janus1.Change2", "weapons/tfa_cso/janus1/change2.wav" )
TFA.AddWeaponSound( "Janus1.Draw", "weapons/tfa_cso/janus1/draw.wav" )

--Bazooka
TFA.AddFireSound( "Bazooka.Fire", "weapons/tfa_cso/bazooka/fire.wav", false, "^" )
TFA.AddFireSound( "Bazooka.Exp1", "weapons/tfa_cso/bazooka/exp1.wav", false, "^" )
TFA.AddFireSound( "Bazooka.Exp2", "weapons/tfa_cso/bazooka/exp2.wav", false, "^" )
TFA.AddFireSound( "Bazooka.Exp3", "weapons/tfa_cso/bazooka/exp3.wav", false, "^" )
TFA.AddWeaponSound( "Bazooka.Draw", "weapons/tfa_cso/bazooka/draw.wav" )
TFA.AddWeaponSound( "Bazooka.ClipOut", "weapons/tfa_cso/bazooka/clipout.wav" )
TFA.AddWeaponSound( "Bazooka.ClipIn", "weapons/tfa_cso/bazooka/clipin.wav" )

--Petrol Boomer
TFA.AddFireSound( "PetrolBoomer.Fire", "weapons/tfa_cso/petrolboomer/fire.wav", false, "^" )
TFA.AddFireSound( "PetrolBoomer.Exp", "weapons/tfa_cso/petrolboomer/exp.wav", false, "^" )
TFA.AddWeaponSound( "PetrolBoomer.Draw", "weapons/tfa_cso/petrolboomer/draw.wav" )
TFA.AddWeaponSound( "PetrolBoomer.Reload", "weapons/tfa_cso/petrolboomer/reload.wav" )
TFA.AddWeaponSound( "PetrolBoomer.Draw_Empty", "weapons/tfa_cso/petrolboomer/draw_empty.wav" )
TFA.AddWeaponSound( "PetrolBoomer.Idle", "weapons/tfa_cso/petrolboomer/idle.wav" )

--Lightning Bazzi-1
TFA.AddFireSound( "CartRed.Fire", "weapons/tfa_cso/cartred/fire.wav", false, "^" )
TFA.AddFireSound( "CartRed.Fire2", "weapons/tfa_cso/cartred/fire2.wav", false, "^" )
TFA.AddWeaponSound( "CartRed.ClipIn", "weapons/tfa_cso/cartred/clipin.wav" )
TFA.AddWeaponSound( "CartRed.ClipOut", "weapons/tfa_cso/cartred/clipout.wav" )
TFA.AddWeaponSound( "CartRed.Foley5", "weapons/tfa_cso/cartred/foley5.wav" )
TFA.AddWeaponSound( "CartRed.HeadOpen", "weapons/tfa_cso/cartred/headopen.wav" )
TFA.AddWeaponSound( "CartRed.HeadClose", "weapons/tfa_cso/cartred/headclose.wav" )

--UMP45 Snake
TFA.AddFireSound( "Snakegun.Fire", "weapons/tfa_cso/snakegun/fire.wav", false, "^" )
TFA.AddWeaponSound( "Snakegun.ClipIn", "weapons/tfa_cso/snakegun/clipin.wav" )
TFA.AddWeaponSound( "Snakegun.ClipOut1", "weapons/tfa_cso/snakegun/clipout1.wav" )
TFA.AddWeaponSound( "Snakegun.ClipOut2", "weapons/tfa_cso/snakegun/clipout2.wav" )
TFA.AddWeaponSound( "Snakegun.Draw", "weapons/tfa_cso/snakegun/draw.wav" )
TFA.AddWeaponSound( "Snakegun.Boltpull", "weapons/tfa_cso/snakegun/boltpull.wav" )

--Lightning Dao-1
TFA.AddFireSound( "CartBlue.Fire", "weapons/tfa_cso/cartblue/fire.wav", false, "^" )
TFA.AddFireSound( "CartBlue.Fire2", "weapons/tfa_cso/cartblue/fire2.wav", false, "^" )
TFA.AddWeaponSound( "CartBlue.ClipIn", "weapons/tfa_cso/cartblue/clipin.wav" )
TFA.AddWeaponSound( "CartBlue.ClipOut", "weapons/tfa_cso/cartblue/clipout.wav" )
TFA.AddWeaponSound( "CartBlue.ClipOut2", "weapons/tfa_cso/cartblue/clipout2.wav" )
TFA.AddWeaponSound( "CartBlue.Foley1", "weapons/tfa_cso/cartblue/foley1.wav" )
TFA.AddWeaponSound( "CartBlue.Foley2", "weapons/tfa_cso/cartblue/foley2.wav" )
TFA.AddWeaponSound( "CartBlue.Foley3", "weapons/tfa_cso/cartblue/foley3.wav" )
TFA.AddWeaponSound( "CartBlue.Foley4", "weapons/tfa_cso/cartblue/foley4.wav" )
TFA.AddWeaponSound( "CartBlue.Draw", "weapons/tfa_cso/cartblue/draw.wav" )
TFA.AddWeaponSound( "CartBlue.Draw2", "weapons/tfa_cso/cartblue/draw2.wav" )
TFA.AddWeaponSound( "CartBlue.Hit", "weapons/tfa_cso/cartblue/hit.wav" )
TFA.AddWeaponSound( "CartBlue.Jump", "weapons/tfa_cso/cartblue/jump.wav" )
TFA.AddWeaponSound( "CartBlue.Spindown", "weapons/tfa_cso/cartblue/spindown.wav" )
TFA.AddWeaponSound( "CartBlue.Turn", "weapons/tfa_cso/cartblue/turn.wav" )
TFA.AddWeaponSound( "CartBlue.Yaho", "weapons/tfa_cso/cartblue/yaho.wav" )

--MP7 Unicorn
TFA.AddFireSound( "Horsegun.Fire", "weapons/tfa_cso/horsegun/fire.wav", false, "^" )
TFA.AddWeaponSound( "Horsegun.ClipIn", "weapons/tfa_cso/horsegun/clipin.wav" )
TFA.AddWeaponSound( "Horsegun.ClipOut", "weapons/tfa_cso/horsegun/clipout.wav" )
TFA.AddWeaponSound( "Horsegun.Idle", "weapons/tfa_cso/horsegun/idle.wav" )
TFA.AddWeaponSound( "Horsegun.Draw", "weapons/tfa_cso/horsegun/draw.wav" )
TFA.AddWeaponSound( "Horsegun.Boltpull", "weapons/tfa_cso/horsegun/boltpull.wav" )

--M95 Ghost Knight
TFA.AddFireSound( "M95Ghost.Fire", "weapons/tfa_cso/m95ghost/fire.wav", false, "^" )
TFA.AddFireSound( "M95Ghost.Fire2", "weapons/tfa_cso/m95ghost/fire2.wav", false, "^" )
TFA.AddWeaponSound( "M95Ghost.ClipIn", "weapons/tfa_cso/m95ghost/clipin.wav" )
TFA.AddWeaponSound( "M95Ghost.ClipOut", "weapons/tfa_cso/m95ghost/clipout.wav" )
TFA.AddWeaponSound( "M95Ghost.Idle", "weapons/tfa_cso/m95ghost/idle.wav" )
TFA.AddWeaponSound( "M95Ghost.Draw", "weapons/tfa_cso/m95ghost/draw.wav" )
TFA.AddWeaponSound( "M95Ghost.Point", "weapons/tfa_cso/m95ghost/point.wav" )
TFA.AddWeaponSound( "M95Ghost.Net1", "weapons/tfa_cso/m95ghost/shoot_net1.wav" )
TFA.AddWeaponSound( "M95Ghost.Net2", "weapons/tfa_cso/m95ghost/shoot_net2.wav" )

--M3 Big Shark
TFA.AddFireSound( "M3Shark.Fire", "weapons/tfa_cso/m3shark/fire.wav", false, "^" )
TFA.AddWeaponSound( "M3Shark.Insert", "weapons/tfa_cso/m3shark/insert.wav" )
TFA.AddWeaponSound( "M3Shark.After_Reload", "weapons/tfa_cso/m3shark/after_reload.wav" )
TFA.AddWeaponSound( "M3Shark.Idle", "weapons/tfa_cso/m3shark/idle.wav" )
TFA.AddWeaponSound( "M3Shark.Draw", "weapons/tfa_cso/m3shark/draw.wav" )

--Newcomen Expert
TFA.AddFireSound( "NewcomenV6.Fire", "weapons/tfa_cso/newcomen_v6/fire.wav", false, "^" )
TFA.AddWeaponSound( "NewcomenV6.Draw", "weapons/tfa_cso/newcomen_v6/draw.wav" )
TFA.AddWeaponSound( "NewcomenV6.Reload", "weapons/tfa_cso/newcomen_v6/reload.wav" )
TFA.AddWeaponSound( "NewcomenV6.Idle", "weapons/tfa_cso/newcomen_v6/idle.wav" )

--Dart Pistol
TFA.AddFireSound( "Dartpistol.Fire", "weapons/tfa_cso/dartpistol/fire.wav", false, "^" )
TFA.AddFireSound( "Dartpistol.Explosion1", "weapons/tfa_cso/dartpistol/explosion1.wav", false, "^" )
TFA.AddFireSound( "Dartpistol.Explosion2", "weapons/tfa_cso/dartpistol/explosion2.wav", false, "^" )
TFA.AddWeaponSound( "Dartpistol.Draw", "weapons/tfa_cso/dartpistol/draw.wav" )
TFA.AddWeaponSound( "Dartpistol.ClipOut1", "weapons/tfa_cso/dartpistol/clipout1.wav" )
TFA.AddWeaponSound( "Dartpistol.ClipOut2", "weapons/tfa_cso/dartpistol/clipout2.wav" )
TFA.AddWeaponSound( "Dartpistol.ClipIn1", "weapons/tfa_cso/dartpistol/clipin1.wav" )
TFA.AddWeaponSound( "Dartpistol.ClipIn2", "weapons/tfa_cso/dartpistol/clipin2.wav" )
TFA.AddWeaponSound( "Dartpistol.ClipIn3", "weapons/tfa_cso/dartpistol/clipin3.wav" )

--Magnum Drill Gold

local soundData = {
	name 		= "MagnumDrill.Draw" ,
	channel 	= CHAN_WEAPON,
	volume 		= 1,
	soundlevel 	= 80,
	pitchstart 	= 100,
	pitchend 	= 100,
	sound 		= "weapons/tfa_cso/magnum_drill/draw.wav"
}

sound.Add(soundData)

local soundData = {
	name 		= "MagnumDrill.ClipOut" ,
	channel 	= CHAN_WEAPON,
	volume 		= 1,
	soundlevel 	= 80,
	pitchstart 	= 100,
	pitchend 	= 100,
	sound 		= "weapons/tfa_cso/magnum_drill/clipout.wav"
}

sound.Add(soundData)

local soundData = {
	name 		= "MagnumDrill.ClipIn" ,
	channel 	= CHAN_WEAPON,
	volume 		= 1,
	soundlevel 	= 80,
	pitchstart 	= 100,
	pitchend 	= 100,
	sound 		= "weapons/tfa_cso/magnum_drill/clipin.wav"
}

sound.Add(soundData)

local soundData = {
	name 		= "MagnumDrill.Idle" ,
	channel 	= CHAN_WEAPON,
	volume 		= 1,
	soundlevel 	= 80,
	pitchstart 	= 100,
	pitchend 	= 100,
	sound 		= "weapons/tfa_cso/magnum_drill/idle.wav"
}

sound.Add(soundData)

local soundData = {
	name 		= "MagnumDrill.Fire" ,
	channel 	= CHAN_USER_BASE+11,
	volume 		= 1,
	soundlevel 	= 80,
	pitchstart 	= 100,
	pitchend 	= 100,
	sound 		= "weapons/tfa_cso/magnum_drill/fire.wav"
}

sound.Add(soundData)

local soundData = {
	name 		= "MagnumDrill.Drill" ,
	channel 	= CHAN_USER_BASE+11,
	volume 		= 1,
	soundlevel 	= 80,
	pitchstart 	= 100,
	pitchend 	= 100,
	sound 		= "weapons/tfa_cso/magnum_drill/drill.wav"
}

sound.Add(soundData)

--Heaven Scorcher
TFA.AddFireSound( "HeavenScorcher.Fire", "weapons/tfa_cso/heaven_scorcher/fire.wav", false, "^" )
TFA.AddFireSound( "HeavenScorcher.Mine_Shoot", "weapons/tfa_cso/heaven_scorcher/mine_shoot.wav", false, "^" )
TFA.AddFireSound( "HeavenScorcher.Mine_Explosion", "weapons/tfa_cso/heaven_scorcher/mine_exp.wav", false, "^" )
TFA.AddFireSound( "HeavenScorcher.Explosion1", "weapons/tfa_cso/heaven_scorcher/exp.wav", false, "^" )
TFA.AddFireSound( "HeavenScorcher.Explosion2", "weapons/tfa_cso/heaven_scorcher/exp2.wav", false, "^" )
TFA.AddWeaponSound( "HeavenScorcher.Draw", "weapons/tfa_cso/heaven_scorcher/draw.wav" )
TFA.AddWeaponSound( "HeavenScorcher.Idle", "weapons/tfa_cso/heaven_scorcher/idle.wav" )
TFA.AddWeaponSound( "HeavenScorcher.BMod_On", "weapons/tfa_cso/heaven_scorcher/bmod_on.wav" )
TFA.AddWeaponSound( "HeavenScorcher.BMod_Exp", "weapons/tfa_cso/heaven_scorcher/bmod_on_exp.wav" )
TFA.AddWeaponSound( "HeavenScorcher.Mine_Set", "weapons/tfa_cso/heaven_scorcher/mine_set.wav" )
TFA.AddWeaponSound( "HeavenScorcher.Mine_Mode", "weapons/tfa_cso/heaven_scorcher/mine_mode.wav" )
TFA.AddWeaponSound( "HeavenScorcher.ClipOut", "weapons/tfa_cso/heaven_scorcher/clipout.wav" )
TFA.AddWeaponSound( "HeavenScorcher.ClipIn", "weapons/tfa_cso/heaven_scorcher/clipin.wav" )

--Mac-10
TFA.AddFireSound( "MAC10.Fire", "weapons/tfa_cso/mac10/fire.wav", false, "^" )
TFA.AddWeaponSound( "MAC10.ClipOut", "weapons/tfa_cso/mac10/clipout.wav")
TFA.AddWeaponSound( "MAC10V2.ClipIn", "weapons/tfa_cso/mac10/clipin_v2.wav")
TFA.AddWeaponSound( "MAC10.ClipRelease", "weapons/tfa_cso/mac10/cliprelease.wav")
TFA.AddWeaponSound( "MAC10.Boltpull", "weapons/tfa_cso/mac10/boltpull.wav")

--P228
TFA.AddFireSound( "P228.Fire", "weapons/tfa_cso/p228/fire.wav", false, "^" )
TFA.AddWeaponSound( "P228.ClipIn", "weapons/tfa_cso/p228/clipin.wav")
TFA.AddWeaponSound( "P228.ClipOut", "weapons/tfa_cso/p228/clipout.wav")
TFA.AddWeaponSound( "P228.Deploy", "weapons/tfa_cso/p228/deploy.wav")
TFA.AddWeaponSound( "P228.SlidePull", "weapons/tfa_cso/p228/slidepull.wav")
TFA.AddWeaponSound( "P228.SlideRelease1", "weapons/tfa_cso/p228/sliderelease1.wav")

--Ballista
TFA.AddWeaponSound( "Ballista.Exp2", "weapons/tfa_cso/ballista/exp2.wav")
TFA.AddWeaponSound( "Ballista.Exp3", "weapons/tfa_cso/ballista/exp3.wav")
TFA.AddWeaponSound( "Ballista.Reload1", "weapons/tfa_cso/ballista/reload1.wav")
TFA.AddWeaponSound( "Ballista.Reload2", "weapons/tfa_cso/ballista/reload2.wav")
TFA.AddWeaponSound( "Ballista.Draw", "weapons/tfa_cso/ballista/draw.wav")
TFA.AddWeaponSound( "Ballista.Missile", "weapons/tfa_cso/ballista/missile.wav")
TFA.AddWeaponSound( "Ballista.Missile_Last", "weapons/tfa_cso/ballista/missile_last.wav")
TFA.AddWeaponSound( "Ballista.Missile_On", "weapons/tfa_cso/ballista/missile_on.wav")

sound.Add({
	['name'] = "Ballista.Exp1",
	['channel'] = CHAN_WEAPON,
	['sound'] = { "weapons/tfa_cso/ballista/exp1.wav"},
	['pitch'] = {100,100}
})
sound.Add({
	['name'] = "Ballista.Missile_Reload",
	['channel'] = CHAN_WEAPON,
	['sound'] = { "weapons/tfa_cso/ballista/missile_reload.wav"},
	['pitch'] = {100,100}
})

local soundData = {
	name 		= "Ballista.Fire" ,
	channel 	= CHAN_USER_BASE+11,
	volume 		= 1,
	soundlevel 	= 80,
	pitchstart 	= 100,
	pitchend 	= 100,
	sound 		= "weapons/tfa_cso/ballista/fire.wav"
}

sound.Add(soundData)

local soundData = {
	name 		= "Ballista.Fire2" ,
	channel 	= CHAN_USER_BASE+11,
	volume 		= 1,
	soundlevel 	= 80,
	pitchstart 	= 100,
	pitchend 	= 100,
	sound 		= "weapons/tfa_cso/ballista/fire2.wav"
}

sound.Add(soundData)

--Heart Bomb

sound.Add({
	['name'] = "Heartbomb.Explode",
	['channel'] = CHAN_USER_BASE,
	['sound'] = { "weapons/tfa_cso/heartbomb/explode.wav"},
	['pitch'] = {100,100}
})

TFA.AddWeaponSound ( "HeartBomb.Pin", "weapons/tfa_cso/heartbomb/pin.wav" )
TFA.AddWeaponSound ( "HeartBomb.Arrow", "weapons/tfa_cso/heartbomb/arrow.wav" )
TFA.AddWeaponSound ( "HeartBomb.Draw", "weapons/tfa_cso/heartbomb/draw.wav" )

--ThunderStorm

sound.Add({
	['name'] = "Thunderstorm.Explode",
	['channel'] = CHAN_USER_BASE,
	['sound'] = { "weapons/tfa_cso/thunderstorm/explode.wav"},
	['pitch'] = {100,100}
})

TFA.AddWeaponSound ( "Thunderstorm.Throw", "weapons/tfa_cso/thunderstorm/throw.wav" )
TFA.AddWeaponSound ( "Thunderstorm.Pullpin", "weapons/tfa_cso/thunderstorm/pullpin.wav" )
TFA.AddWeaponSound ( "Thunderstorm.Idle", "weapons/tfa_cso/thunderstorm/idle.wav" )
TFA.AddWeaponSound ( "Thunderstorm.Draw", "weapons/tfa_cso/thunderstorm/draw.wav" )
TFA.AddWeaponSound ( "Thunderstorm.Charge", "weapons/tfa_cso/thunderstorm/charge.wav" )